var app = angular.module('MyApp', ['ngRoute'])
var _details={};
 var fullList=[];

app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
                    when('/:param', {
                        templateUrl: 'viewCountry.htm',
                        controller: 'RouteController'
                    });
                   
}]);

function findME(fullList,name){

    for(var key in fullList){
        
        if (fullList[key].country==name)
            return fullList[key];
    }
    
}


app.controller("CountryCtrl", function($scope, $rootScope, $http) {

      $http.get('http://country.io/names.json').
        success(function(data, status, headers, config) {
          $scope.country= data;
          $scope.COUNTRIES=[];
          for(var cap in data){
           $scope.COUNTRIES.push(data[cap]);
          }
          
       

          $http.get('http://country.io/continent.json').
              success(function(data, status, headers, config) {
                $scope.continent = data; 
            
      
              $http.get('http://country.io/iso3.json').
                  success(function(data, status, headers, config) {
                    $scope.iso3 = data; 
                                 

                    $http.get('http://country.io/capital.json').
                      success(function(data, status, headers, config) {
                        $scope.capital = data;


                          $http.get('http://country.io/phone.json').
                            success(function(data, status, headers, config) {
                              $scope.phone = data;
                        
        
                                    $http.get('http://country.io/currency.json').
                                        success(function(data, status, headers, config) {
                                          $scope.currency = data;

                                          for(var key in $scope.country){
                                              fullList[key]={};
                                              fullList[key].code=key;
                                              fullList[key].iso=$scope.iso3[key];
                                              fullList[key].country=$scope.country[key];
                                              fullList[key].contient=$scope.continent[key];
                                              fullList[key].currency=$scope.currency[key];
                                              fullList[key].phone=$scope.phone[key];
                                              fullList[key].capital=$scope.capital[key];

                                          }    
          
                                            //console.log($rootScope.route_param);
                                            //console.log(fullList);
                                            $scope.$$childHead.para=findME(fullList,$rootScope.route_param);
                                           // console.log($scope.$$childHead.para);
                                            //console.log("okay");
                                           //console.log($scope.para);

                                    }).
                                    error(function(data, status, headers, config) {
                                      console.log("Currency Error!");
                                    });
                          }).
                            error(function(data, status, headers, config) {
                              console.log("Phone Error!");
                            });

                        }).
                          error(function(data, status, headers, config) {
                            console.log("Capital Error!");
                          });

                    }).
                    error(function(data, status, headers, config) {
                      console.log("Iso3 Error!");
                    });

              }).
              error(function(data, status, headers, config) {
                console.log("Continent Error!");
              });

           }).
        error(function(data, status, headers, config) {
          console.log("Country Error!");
        });
 

});

app.controller("RouteController", function($scope,$rootScope, $routeParams,$http) {

  
  $rootScope.route_param = $routeParams.param;
  // console.log($routeParams.param);
  var object=findME(fullList,$routeParams.param);
  $scope.para=object;

});


