"use strict";
const r=require("rethinkdb");
const co=require("co");
var _conn;
var _dbN;


class DB{

    constructor(dbName){
        this.conn=_conn;
        this.dbN=_dbN;
        
    }

    InitConnection(dbName){
        let me=this;
        
        return co(function*(){
            try{
                if(!_conn){
                        _conn= yield r.connect( {host: 'localhost', db:dbName, port: 28015})
                        me.conn=_conn;
                        me.dbN=dbName;
                }

            }
            catch(err){
                console.log(err);
            }
            
        });
    }

    toInsert(tableName,body){

        let me=this;
      
        return co(function*(){
            try{
                
                if(me.conn){
                        delete body.tbN;
                        var k = yield r.uuid().run(me.conn,function(err,ied){body.id=ied;console.log("hello");});
                        var l= yield r.table(tableName).insert(body).run(me.conn);
                         return JSON.stringify(body) + "\n\n\nINSERTED SUCCESSFULLY!"
                }
                
            }
            
            catch(err){
                console.log(err);
            }
        });

       
    }
    


    readSnipsAll(tbN,indexI,id){
    
         var arra=[];
         let me=this;
          return co(function*(){
                try{
                        var pr=yield r.table(tbN).getAll(id, {index: indexI}).run(me.conn);
                        
                        var mm= yield pr.toArray();
                                return mm;

                    }
                
                    catch(err){
                         console.log(err);
                }
            });
    }

     readSnipsOne(tbN,id){
    
         let me=this;
          return co(function*(){
                try{
                        var pr=yield r.table(tbN).get(id).run(me.conn);
                        if(!pr)
                            return "NOT FOUND!"
                        return pr;

                    }
                    catch(err){
                         console.log(err);
                }
            });
    }

    editSnips(tbN,uid,body){

        let me=this;
          return co(function*(){
                try{
                        var pr=yield r.table(tbN).get(uid).update({"language":body.language,"snip":body.snippet}).run(me.conn);
                        return "Successfully edited!"

                    }
                    catch(err){
                         console.log(err);
                }
            });
    }




    deleteSnips(tbN,uid){

        let me=this;
          return co(function*(){
                try{
                        var pr=yield r.table(tbN).get(uid).delete().run(me.conn);
                        return "Successfully Deleted!"

                    }
                    catch(err){
                         console.log(err);
                }
            });
    }





}

module.exports= DB;