"use strict";
var dbs = require('./dbConnection.js');
const co=require("co");
class Snippet{

    constructor(){
        this.db=new dbs("eg1");
        this.db.InitConnection("eg1").then((res)=>{
             //   console.log("DB Initiated");
                
    
        },(err)=> {
            console.log(err);
        });
        
       // console.log("Snippet Initiated");

    }

    toInsert(id,body){
        body.IDauthor=id;
        console.log(this);
        return this.db.toInsert(body.tbN,body);
        
    }

    toReadAll(id){

        return this.db.readSnipsAll("userData","IDauthor",id);
    }

     toReadOne(id){

        return this.db.readSnipsOne("userData",id);
    }

    toEditOne(id,body){

        return this.db.editSnips("userData",id,body);
    }

    toDeleteOne(id){

        return this.db.deleteSnips("userData",id);
    }


}

module.exports = Snippet
