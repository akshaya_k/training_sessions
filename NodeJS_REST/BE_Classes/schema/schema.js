const Joi = require('joi');
const header = Joi.object({
    'userid' : Joi.string().required()
}).unknown(true);

const body = Joi.object({

    'language' : Joi.string().valid('c++','c','javascript','python').required(),
    'snippet' : Joi.string().required(),
    'id': Joi.string(),
    'tbN' : Joi.string().required()
});

const param = Joi.object({

    'id' : Joi.string().required()
});

module.exports = {

    header,
    body,
    param

}