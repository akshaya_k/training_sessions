'use strict';

const Hapi = require('hapi');
const Joi = require('joi');
const fs = require('fs');
const schema = require('../schema/schema.js');
var Snippet = require('../Snippets.js');
var snips=new Snippet();

module.exports= [{
    method: 'GET',
    path: '/',
    handler: function (request, reply) {
        reply(snips.toReadAll(request.headers["userid"]));  
    },
    config: {
        validate:{
            headers: schema.header,
        }
    }
},
   {
    method: 'GET',
    path: '/{id}',
    handler: function (request, reply) {
       reply(snips.toReadOne(request.params.id));  
    },
    config: {
        validate:{
            headers: schema.header,
            params:schema.param
        }
    }
},

{
    method: 'POST',
    path: '/',
    handler: function (request, reply) {
        var abc= request.payload;
        reply(snips.toInsert(request.headers["userid"],abc));
    },
    config: {
        validate:{
            headers: schema.header,
            payload: schema.body
        }
    }
},
{
    method: 'PUT',
    path: '/{id}',
    handler: function (request, reply) {
        var abc= request.payload;
       reply(snips.toEditOne(request.params.id,abc));
    },
    config: {
        validate:{
            headers: schema.header,
            params:schema.param,
            payload:schema.body
        }
    }
},
{
    method: 'DELETE',
    path: '/{id}',
    handler: function (request, reply) {
        reply(snips.toDeleteOne(request.params.id));
    },
    config: {
        validate:{
            headers: schema.header,
            params:schema.param
        }
    }
}];


