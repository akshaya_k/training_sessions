'use strict';

const Hapi = require('hapi');
const Joi = require('joi');
const fs = require('fs');
const schema = require('../schema/schema.js');
//const handler = require('../handler.js');
const handler=require('../dbConnection.js');

module.exports= [{
    method: 'GET',
    path: '/',
    handler: function (request, reply) {
        handler.readSnipsAll(request.headers["userid"],reply); 
    },
    config: {
        validate:{
            headers: schema.header,
        }
    }
},
   {
    method: 'GET',
    path: '/{id}',
    handler: function (request, reply) {
        handler.readSnipsOne(request.headers["userid"],request.params.id,reply);
    },
    config: {
        validate:{
            headers: schema.header,
            params:schema.param
        }
    }
},

{
    method: 'POST',
    path: '/',
    handler: function (request, reply) {
        var abc= request.payload;
        handler.addSnips(request.headers["userid"],abc,reply);
    },
    config: {
        validate:{
            headers: schema.header,
            payload: schema.body
        }
    }
},
{
    method: 'PUT',
    path: '/{id}',
    handler: function (request, reply) {
        var abc= request.payload;
        handler.editSnips(request.params.id,abc,reply);
    },
    config: {
        validate:{
            headers: schema.header,
            params:schema.param,
            payload:schema.body
        }
    }
},
{
    method: 'DELETE',
    path: '/{id}',
    handler: function (request, reply) {
        handler.removeSnips(request.params.id,reply);
    },
    config: {
        validate:{
            headers: schema.header,
            params:schema.param
        }
    }
}];


