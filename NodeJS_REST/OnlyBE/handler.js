

const Hapi = require('hapi');
const Joi = require('joi');
const fs = require('fs');
const dataAccess=require('./dataAccess.js');

readUsers= ()=>{
   var userS=fs.readFileSync("userInfo.json","utf8");
    var userJSON=JSON.parse(userS);
    var listOf=userJSON.users;
    console.log(userJSON);
    return userJSON;

}

isValid= (listOf,userid)=>{

    var count=0;
   // console.log(listOf);
    for(var i=0; i<listOf.length;i++){
        if (userid!=listOf[i].userid){
            count++;
        }
        else
            return(i);
    }
        
    return -1;
}

readSnips=(listOf,id,para)=>{

  
    var countc=0;
    
    var i=isValid(listOf,id);


    if(i==-1)
        return "User Not Found";


    if(para=="No")
        return listOf[i].codes;

    for(var j=0; j<listOf[i].codes.length;j++)
        if(listOf[i].codes[j]==null)
            continue;
       else if (para==listOf[i].codes[j].id)
            return listOf[i].codes[j];   
    

    return "Code Snippet Not Found";
}


addSnips=(listOf,userid,snip)=>{

    var i=isValid(listOf,userid);
    if(i==-1)
        return "User Not Found";
//console.log(JSON.stringify(listOf));
    listOf[i].curL=Number(listOf[i].curL)+1;
    snip['id']=listOf[i].curL;
    listOf[i].codes.push(snip);
    fs.writeFile("userInfo.json",JSON.stringify(listOf));
    return listOf[i].codes;

}

findcodeid=(codes,i,id)=>{

    for(var j=0;j<codes.length;j++)
        if(codes[j].id==id)
            return j;

    return -1;
}

editSnips=(listOf,userid,id,snip)=>{

    var i=isValid(listOf,userid);
    if(i==-1)
        return "User Not Found";
//console.log(JSON.stringify(listOf));
    var j=findcodeid(listOf[i].codes,i,id);
    if(j==-1)
        return "Code Snippet Not Found";

    listOf[i].codes[j].language=snip.language;
    listOf[i].codes[j].snip=snip.snip;
    listOf[i].codes[j].id=snip.id == '' ? id : snip.id ;
    
    fs.writeFile("userInfo.json",JSON.stringify(listOf));
    return listOf[i].codes[j];

}


removeSnips=(listOf,userid,codeid)=>{

    
    var i=isValid(listOf,userid);
    if(i==-1)
        return "User Not Found";
    
    var count=0;

    for(var j=0; j<listOf[i].codes.length;j++)

        if (codeid!=listOf[i].codes[j].id)
            count++;
        else 
            break;

    if(count==listOf[i].codes.length)
        return "Snip Not Found";

    listOf[i].codes.splice(j,1);
    fs.writeFile("userInfo.json",JSON.stringify(listOf));
    return listOf[i].codes;
}

module.exports={

    readSnips,
    readUsers,
    editSnips,
    removeSnips,
    findcodeid,
    isValid,
    addSnips

};