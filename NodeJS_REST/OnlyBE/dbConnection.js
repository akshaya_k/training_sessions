"use strict";
const  co = require('co');
const r=require('rethinkdb');
let conn;
 r.connect( {host: 'localhost', db:'eg1', port: 28015} ).then(function(conns){conn=conns});

function addSnips(id,body,reply){

        var k=r.uuid().run(conn,function(err,ied){
            if(err) 
                throw err; 
            
            body.id=ied;
            body.IDauthor=id;
            
            r.table("userData").insert(body).run(conn,function(err,ied){
                
                if(err) throw err; 
            
            });
            
            reply(JSON.stringify(body) + "\n\n\nINSERTED SUCCESSFULLY!");
            
        });
}
    

function readSnipsAll(id,reply){

var k;
var arra=[];
        var x=r.table('userData').getAll(id, {index:'IDauthor'}).run(conn,function(err,cursor){
           cursor.each(function(err, row) {
                if (err) throw err;
                    arra.push(row);
                    console.log(arra);
            });

            reply(arra);
           
        });
}


function readSnipsOne(id,uid,reply){

var k;
        var x=r.table('userData').get(uid).run(conn,function(err,cursor){

            reply(cursor);           
        });
}


function removeSnips(uid,reply){

var k;
      k=r.table("userData").get(uid).delete().run(conn,function(err,conn){

            if(err)
                throw err;
            reply("DELETED SUCCESSFULLY");

      });
}




function editSnips(uid,body,reply){

var k;
       k=r.table("userData").get(uid).update({"language":body.language,"snip":body.snippet}).run(conn);
                var x=r.table('userData').get(uid).run(conn,function(err,cursor){

                    reply(JSON.stringify(cursor) + "\n\n\nINSERTED SUCCESSFULLY!");       
              });
}

module.exports = {addSnips,removeSnips,editSnips,readSnipsAll,readSnipsOne};