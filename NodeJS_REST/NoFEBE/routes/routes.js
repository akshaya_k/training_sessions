'use strict';

const Hapi = require('hapi');
const Joi = require('joi');
const fs = require('fs');
const schema = require('../schema/schema.js');
const handler = require('../handler.js');

module.exports= [{
    method: 'GET',
    path: '/',
    handler: function (request, reply) {
        var listOf=handler.readUsers();
        reply(handler.readSnips(listOf,request.headers["userid"],"No")); 
    },
    config: {
        validate:{
            headers: schema.header,
        }
    }
},
   {
    method: 'GET',
    path: '/{id}',
    handler: function (request, reply) {
        var listOf=handler.readUsers();
        reply(handler.readSnips(listOf,request.headers["userid"],request.params.id));
    },
    config: {
        validate:{
            headers: schema.header,
            params:schema.param
        }
    }
},

{
    method: 'POST',
    path: '/',
    handler: function (request, reply) {
        var abc= request.payload;
        var listOf=handler.readUsers();
        reply(handler.addSnips(listOf,request.headers["userid"],abc));
    },
    config: {
        validate:{
            headers: schema.header,
            payload: schema.body
        }
    }
},
{
    method: 'PUT',
    path: '/{id}',
    handler: function (request, reply) {
        var abc= request.payload;
        var listOf=handler.readUsers();
        
        reply(handler.editSnips(listOf,request.headers["userid"],request.params.id,abc));
    },
    config: {
        validate:{
            headers: schema.header,
            params:schema.param,
            payload:schema.body
        }
    }
},
{
    method: 'DELETE',
    path: '/{id}',
    handler: function (request, reply) {
        var abc= request.payload;
        var listOf=handler.readUsers();
        reply(handler.removeSnips(listOf,request.headers["userid"],request.params.id));
    },
    config: {
        validate:{
            headers: schema.header,
            params:schema.param
        }
    }
}];


