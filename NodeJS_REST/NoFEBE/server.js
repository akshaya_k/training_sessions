'use strict';


const Hapi = require('hapi');
const Joi = require('joi');
const fs = require('fs');
const Routes=require('./routes/routes.js');



// Create a server with a host and port
const server = new Hapi.Server();

server.connection({ 
    host: 'localhost', 
    port: 3000 
});



server.route(Routes);
server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});